import { useState } from 'react';
import axios from 'axios';

export default function Home() {
  const [resumes, setResumes] = useState([]);
  const [jobCriteria, setJobCriteria] = useState('');
  const [topResumes, setTopResumes] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const resumeFiles = Array.from(resumes);
    const resumePromises = resumeFiles.map(file => {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result);
        reader.onerror = reject;
        reader.readAsText(file);
      });
    });

    try {
      const resumeContents = await Promise.all(resumePromises);

      const response = await axios.post('/api/screenResumes', { resumes: resumeContents, jobCriteria });

      if (response.status === 200) {
        setTopResumes(response.data.topResumes);
      } else {
        console.error('Error screening resumes:', response.data);
        alert('There was an error processing your request. Please try again.');
      }
    } catch (error) {
      console.error('Error screening resumes:', error);
      alert('There was an error processing your request. Please try again.');
    } finally {
      setLoading(false);
    }
  };

  const handleResumeChange = (e) => {
    setResumes([...e.target.files]);
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded shadow-md max-w-md w-full">
        <h1 className="text-2xl font-bold mb-6 text-center">Resume Screener</h1>
        <form className="space-y-6" onSubmit={handleSubmit}>
          <div>
            <label className="block text-sm font-medium text-gray-700">Job Criteria:</label>
            <textarea
              className="mt-1 block w-full p-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
              value={jobCriteria}
              onChange={(e) => setJobCriteria(e.target.value)}
              required
            />
          </div>
          <div>
            <label className="block text-sm font-medium text-gray-700">Resumes:</label>
            <input
              type="file"
              multiple
              className="mt-1 block w-full p-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
              onChange={handleResumeChange}
              required
            />
          </div>
          <button
            type="submit"
            className="w-full py-2 px-4 bg-indigo-600 text-white font-bold rounded-md hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            disabled={loading}
          >
            {loading ? 'Screening...' : 'Screen Resumes'}
          </button>
        </form>
        {topResumes.length > 0 && (
          <div className="mt-8">
            <h2 className="text-xl font-bold mb-4 text-center">Top 3 Resumes</h2>
            <ul className="space-y-4">
              {topResumes.map((resume, index) => (
                <li key={index} className="p-4 border border-gray-300 rounded-md">
                  {resume}
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </div>
  );
}

