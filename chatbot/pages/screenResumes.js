import { OpenAIApi } from 'openai';

const openai = new OpenAIApi({
  apiKey: process.env.OPENAI_API_KEY,
});

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { resumes, jobCriteria } = req.body;

    try {
      const formattedResumes = resumes.map((resume, index) => `Resume ${index + 1}:\n${resume}`).join('\n\n');
      const prompt = `Job Criteria:\n${jobCriteria}\n\nResumes:\n${formattedResumes}\n\nSelect the top 3 resumes that best match the job criteria and list them.`;

      const response = await openai.createChatCompletion({
        model: 'gpt-4',
        messages: [{ role: 'user', content: prompt }],
      });

      const topResumesText = response.data.choices[0].message.content.trim();
      const topResumes = topResumesText.split('\n\n').slice(0, 3); // Adjust according to the response format

      res.status(200).json({ topResumes });
    } catch (error) {
      console.error('Error processing resumes:', error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  } else {
    res.status(405).json({ error: 'Method Not Allowed' });
  }
}

