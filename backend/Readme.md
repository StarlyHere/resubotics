# Resume Parsing Backend

## Installation
Requirements :
- Python V3.10 & above
- Latest compatible PIP


## Instructions
- Clone Repository
  - ```cd resubotics/backend```
  - ```python3 -m venv venv ```
  - In Windows 
    - ```venv/scripts/activate```
  - In Linux / Mac 
    - ``` source venv/bin/activate```
- Install dependencies
```bash
pip install -r requirements.txt
```
- Start Flask Server 
```bash
gunicorn -w 4 -b 0.0.0.0:8000 app:app
```

## Testing in PostMan
- Create New Request 
- Select Type POST   
- Set the Below URL
```url
http://localhost:8000/upload_pdf
```
- Go to Body and select FormData 
- In key --> pdf_file
- From dropdown select File option
- In value Upload the PDF file

### Send Request

## Response Format 
```json
{
    "languages":[],
    "name":"",
    "skills":[]
}
```