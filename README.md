# ResuBotics

**ResuBotics** is a comprehensive solution designed to assist with all your resume-related needs. Whether you're a recruiter looking to find the best candidates or a job seeker aiming to craft the perfect resume, ResuBotics has got you covered. Our tool offers three primary features:

- **Resume Chatbot**: Enables recruiters to upload multiple resumes and use a chatbot to shortlist the best-fit resumes based on specific requirements.
- **Resume Maker**: Allows users to enter their details into a form and generate a LaTeX resume that is converted to a PDF and opened in a new browser window.
- **Resume Analyzer**: Helps users improve their resumes by analyzing them against a job opening, highlighting areas for improvement based on keyword matching.

## Features

### Resume Chatbot

The Resume Chatbot feature is designed for recruiters who need to sift through numerous resumes efficiently. Recruiters can:

- Upload resumes of several applicants.
- Interact with a chatbot to specify the criteria for shortlisting candidates.
- Receive recommendations on the best-fit resumes based on their needs.

### Resume Maker

The Resume Maker feature simplifies the resume creation process for users. Users can:

- Enter their personal, educational, and professional details into a form.
- Automatically generate a LaTeX-formatted resume.
- View and download the resume as a PDF in a new browser window.

### Resume Analyzer

The Resume Analyzer feature is aimed at helping job seekers refine their resumes to better match job openings. Users can:

- Upload their current resume.
- Enter details about the job opening they are targeting.
- Receive a detailed analysis highlighting where their resume lacks, with suggestions for improvement based on keyword matching with the job description.

## Getting Started

To run ResuBotics locally, follow these steps:

1. Clone the repository:
   ```bash
   git clone https://github.com/yourusername/resubotics.git
   cd resubotics
2. Install Dependencies:
    ```bash 
    npm install
3. Start the development server:
    ```bash
    npm run dev


